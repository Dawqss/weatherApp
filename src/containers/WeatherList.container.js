import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Sparklines, SparklinesLine } from 'react-sparklines';


class WeatherList extends Component {
    renderWeather(cityData) {
        console.log(cityData);
        const temps = cityData.list.map(weather => weather.main.temp);
        const humidity = cityData.list.map(weather => weather.main.humidity);
        const pressure = cityData.list.map(weather => weather.main.pressure);
        return (
            <tr>
                <td>
                    {cityData.city.name}
                </td>
                <td>
                    <Sparklines data={temps} height={100}>
                        <SparklinesLine color="red"/>
                    </Sparklines>
                </td>
                <td>
                    <Sparklines data={humidity} height={100}>
                        <SparklinesLine color="blue"/>
                    </Sparklines>
                </td>
                <td>
                    <Sparklines data={pressure} height={100}>
                        <SparklinesLine color="green"/>
                    </Sparklines>
                </td>
            </tr>
        )
    }
    render() {
         return (
             <table className="table table-hover">
                 <thead>
                 <tr>
                     <th>City</th>
                     <th>Temperature</th>
                     <th>Pressure</th>
                     <th>Humidity</th>
                 </tr>
                 </thead>
                 <tbody>
                    {this.props.weather.map(this.renderWeather)}
                 </tbody>
             </table>
         )
    }
}

function mapStateToProps(state) {
    return {
        weather: state.weather
    }
}

export default connect(mapStateToProps)(WeatherList);