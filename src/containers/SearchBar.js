import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from "../actions";


class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = {term: ''}

        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onInputChange(event) {
        this.setState({term: event.target.value})
    }

    onFormSubmit(event) {
        event.preventDefault();
        this.props.fetchWeather(this.state.term);
        this.setState({term: ''})
    }

    handlingError(error) {
        return (
            <div>
                <h5>ERROR</h5>
            </div>
        )
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onFormSubmit}  className="input-group mb-3 mt-3">
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Search term"
                        value={this.state.term}
                        onChange={this.onInputChange}/>
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="submit">Button</button>
                    </div>
                </form>
            </div>

        )
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchWeather }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);


